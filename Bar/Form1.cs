﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bar
{
    public partial class Form1 : Form
    {

        private Copo meuCopo;
        private bool servico;

        public Form1()
        {
            InitializeComponent();
            servico = false; // para ser utilizado no ButtonIniciarServico verificando em que fase está
            meuCopo = new Copo();
                 
            
        }

        private void ButtonIniciarServico_Click(object sender, EventArgs e)
        {
            double capacidade;
            if (!servico)
            {
                if (ComboBoxLiquido.Text == "") // Verifica se foi escolhido um liquido
                {
                    LabelBebida.Text = "Falta escolher o tipo de bebida.";
                }
                else if (ComboBoxCapacidade.Text == "") // Verifica se foi escolhido uma quantidade
                {

                    LabelBebida.Text = "Falta escolher a capacidade do copo.";

                }
                else
                {
                    capacidade = Convert.ToDouble(ComboBoxCapacidade.Text); // Converter o valor da ComboBoxCapacidade
                    meuCopo.Capacidade = capacidade; // Passar o valor da capacidade para o atributo Capacidade 
                    meuCopo.Liquido = ComboBoxLiquido.Text; // Passar o valor da ComboBoxLiquido para o atributo Liquido
                    servico = true;

                    // Ativar as ComboBox
                    ComboBoxLiquido.Enabled = false;
                    ComboBoxCapacidade.Enabled = false;
                    // Desativar os butões
                    ButtonEncher.Enabled = true;
                    ButtonEsvaziar.Enabled = true;
                    ButtonEncherTudo.Enabled = true;
                    ButtonEsvaziarTudo.Enabled = true;
                    // Atribuir ou alterar dados dos botões e Labels
                    ButtonIniciarServico.Text = "Terminar serviço...";
                    LabelBebida.Text = meuCopo.Liquido;
                    LabelCapacidadeUsada.Text = meuCopo.Contem.ToString() + " cl / " + meuCopo.Capacidade.ToString() + " cl";
                    ButtonEncherTudo.Text = "Encher (" + (meuCopo.Capacidade - meuCopo.Contem) + "cl)";
                    ButtonEsvaziarTudo.Text = "Esvaziar (" + (meuCopo.Contem) + "cl)";
                    

                }
            }
            else
            {
                servico = false;
                // Desativar as ComboBox
                ComboBoxLiquido.Enabled = true;
                ComboBoxCapacidade.Enabled = true;
                // Ativar os butões
                ButtonEncher.Enabled = false;
                ButtonEsvaziar.Enabled = false;
                ButtonEncherTudo.Enabled = false;
                ButtonEsvaziarTudo.Enabled = false;
                // Atribuir ou alterar dados dos botões e Labels
                ComboBoxLiquido.Text = "";
                ComboBoxCapacidade.Text = "";
                ButtonIniciarServico.Text = "Iniciar serviço...";
                LabelBebida.Text = "Bebida...";
                LabelCapacidadeUsada.Text = "- cl / - cl";
                ButtonEncherTudo.Text = "Encher (0 cl)";
                ButtonEsvaziarTudo.Text = "Esvaziar (0 cl)";
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_1_1;

            }
        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            meuCopo.Encher(1);
            // Alterar dados dos botões e Labels
            LabelCapacidadeUsada.Text = meuCopo.Contem.ToString() + " cl / " + meuCopo.Capacidade.ToString() + " cl";
            ButtonEncherTudo.Text = "Encher (" + (meuCopo.Capacidade - meuCopo.Contem) + "cl)";
            ButtonEsvaziarTudo.Text = "Esvaziar (" + (meuCopo.Contem) + "cl)";
            AlterarImagem();
        }

        private void ButtonEsvaziar_Click(object sender, EventArgs e)
        {
            meuCopo.Esvaziar(1);
            // Alterar dados dos botões e Labels
            LabelCapacidadeUsada.Text = meuCopo.Contem.ToString() + " cl / " + meuCopo.Capacidade.ToString() + " cl";
            ButtonEncherTudo.Text = "Encher (" + (meuCopo.Capacidade - meuCopo.Contem) + "cl)";
            ButtonEsvaziarTudo.Text = "Esvaziar (" + (meuCopo.Contem) + "cl)";
            AlterarImagem();
        }

        private void ButtonEncherTudo_Click(object sender, EventArgs e)
        {
            double faltaEncher;

            faltaEncher = meuCopo.Capacidade - meuCopo.Contem; // Saber a quantidade que falta encher

            meuCopo.Encher(faltaEncher);
            // Alterar dados dos botões e Labels
            LabelCapacidadeUsada.Text = meuCopo.Contem.ToString() + " cl / " + meuCopo.Capacidade.ToString() + " cl";
            ButtonEncherTudo.Text = "Encher (" + (meuCopo.Capacidade - meuCopo.Contem) + "cl)";
            ButtonEsvaziarTudo.Text = "Esvaziar (" + (meuCopo.Contem) + "cl)";
            AlterarImagem();

        }

        private void ButtonEsvaziarTudo_Click(object sender, EventArgs e)
        {
            meuCopo.Esvaziar(meuCopo.Contem);
            // Alterar dados dos botões e Labels
            LabelCapacidadeUsada.Text = meuCopo.Contem.ToString() + " cl / " + meuCopo.Capacidade.ToString() + " cl";
            ButtonEncherTudo.Text = "Encher (" + (meuCopo.Capacidade - meuCopo.Contem) + "cl)";
            ButtonEsvaziarTudo.Text = "Esvaziar (" + (meuCopo.Contem) + "cl)";
            AlterarImagem();
        }

        private void AlterarImagem ()
        {
         
            if (meuCopo.ValorEmPercentagem() >= 0 && meuCopo.ValorEmPercentagem() <= 25)
            {
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_1_1;
            }
            else if (meuCopo.ValorEmPercentagem() > 25 && meuCopo.ValorEmPercentagem() <= 50)
            {
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_2_1;
            }
            else if (meuCopo.ValorEmPercentagem() > 50 && meuCopo.ValorEmPercentagem() <= 75)
            {
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_3_1;
            }
            else if (meuCopo.ValorEmPercentagem() > 75 && meuCopo.ValorEmPercentagem() <= 90)
            {
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_4_1;
            }
            else
            {
                this.pictureBox1.Image = global::Bar.Properties.Resources.copo_5_1;
            }
        }
    }
}
