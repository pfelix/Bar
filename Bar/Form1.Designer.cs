﻿namespace Bar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelLiquido = new System.Windows.Forms.Label();
            this.LabelCapacidadeUsada = new System.Windows.Forms.Label();
            this.ButtonIniciarServico = new System.Windows.Forms.Button();
            this.LabelCapacidade = new System.Windows.Forms.Label();
            this.ButtonEsvaziar = new System.Windows.Forms.Button();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.LabelBebida = new System.Windows.Forms.Label();
            this.LabelVolume = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonEsvaziarTudo = new System.Windows.Forms.Button();
            this.ButtonEncherTudo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ComboBoxLiquido = new System.Windows.Forms.ComboBox();
            this.ComboBoxCapacidade = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelLiquido
            // 
            this.LabelLiquido.AutoSize = true;
            this.LabelLiquido.Location = new System.Drawing.Point(54, 18);
            this.LabelLiquido.Name = "LabelLiquido";
            this.LabelLiquido.Size = new System.Drawing.Size(40, 13);
            this.LabelLiquido.TabIndex = 0;
            this.LabelLiquido.Text = "Bebida";
            // 
            // LabelCapacidadeUsada
            // 
            this.LabelCapacidadeUsada.AutoSize = true;
            this.LabelCapacidadeUsada.BackColor = System.Drawing.SystemColors.Control;
            this.LabelCapacidadeUsada.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCapacidadeUsada.Location = new System.Drawing.Point(29, 135);
            this.LabelCapacidadeUsada.Name = "LabelCapacidadeUsada";
            this.LabelCapacidadeUsada.Size = new System.Drawing.Size(36, 12);
            this.LabelCapacidadeUsada.TabIndex = 12;
            this.LabelCapacidadeUsada.Text = "- cl / - cl";
            this.LabelCapacidadeUsada.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ButtonIniciarServico
            // 
            this.ButtonIniciarServico.Location = new System.Drawing.Point(136, 68);
            this.ButtonIniciarServico.Name = "ButtonIniciarServico";
            this.ButtonIniciarServico.Size = new System.Drawing.Size(101, 23);
            this.ButtonIniciarServico.TabIndex = 5;
            this.ButtonIniciarServico.Text = "Iniciar serviço...";
            this.ButtonIniciarServico.UseVisualStyleBackColor = true;
            this.ButtonIniciarServico.Click += new System.EventHandler(this.ButtonIniciarServico_Click);
            // 
            // LabelCapacidade
            // 
            this.LabelCapacidade.AutoSize = true;
            this.LabelCapacidade.Location = new System.Drawing.Point(31, 46);
            this.LabelCapacidade.Name = "LabelCapacidade";
            this.LabelCapacidade.Size = new System.Drawing.Size(64, 13);
            this.LabelCapacidade.TabIndex = 2;
            this.LabelCapacidade.Text = "Capacidade";
            // 
            // ButtonEsvaziar
            // 
            this.ButtonEsvaziar.Enabled = false;
            this.ButtonEsvaziar.Location = new System.Drawing.Point(129, 62);
            this.ButtonEsvaziar.Name = "ButtonEsvaziar";
            this.ButtonEsvaziar.Size = new System.Drawing.Size(89, 23);
            this.ButtonEsvaziar.TabIndex = 9;
            this.ButtonEsvaziar.Text = "Esvaziar (1 cl)";
            this.ButtonEsvaziar.UseVisualStyleBackColor = true;
            this.ButtonEsvaziar.Click += new System.EventHandler(this.ButtonEsvaziar_Click);
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Enabled = false;
            this.ButtonEncher.Location = new System.Drawing.Point(129, 36);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(89, 23);
            this.ButtonEncher.TabIndex = 8;
            this.ButtonEncher.Text = "Encher (1 cl)";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // LabelBebida
            // 
            this.LabelBebida.AutoSize = true;
            this.LabelBebida.Location = new System.Drawing.Point(17, 17);
            this.LabelBebida.Name = "LabelBebida";
            this.LabelBebida.Size = new System.Drawing.Size(49, 13);
            this.LabelBebida.TabIndex = 7;
            this.LabelBebida.Text = "Bebida...";
            this.LabelBebida.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LabelVolume
            // 
            this.LabelVolume.AutoSize = true;
            this.LabelVolume.Location = new System.Drawing.Point(214, 45);
            this.LabelVolume.Name = "LabelVolume";
            this.LabelVolume.Size = new System.Drawing.Size(15, 13);
            this.LabelVolume.TabIndex = 4;
            this.LabelVolume.Text = "cl";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonEsvaziarTudo);
            this.groupBox1.Controls.Add(this.ButtonEncherTudo);
            this.groupBox1.Controls.Add(this.LabelBebida);
            this.groupBox1.Controls.Add(this.ButtonEncher);
            this.groupBox1.Controls.Add(this.ButtonEsvaziar);
            this.groupBox1.Controls.Add(this.LabelCapacidadeUsada);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(19, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 149);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serviço";
            // 
            // ButtonEsvaziarTudo
            // 
            this.ButtonEsvaziarTudo.Enabled = false;
            this.ButtonEsvaziarTudo.Location = new System.Drawing.Point(129, 114);
            this.ButtonEsvaziarTudo.Name = "ButtonEsvaziarTudo";
            this.ButtonEsvaziarTudo.Size = new System.Drawing.Size(89, 23);
            this.ButtonEsvaziarTudo.TabIndex = 11;
            this.ButtonEsvaziarTudo.Text = "Esvaziar (0 cl)";
            this.ButtonEsvaziarTudo.UseVisualStyleBackColor = true;
            this.ButtonEsvaziarTudo.Click += new System.EventHandler(this.ButtonEsvaziarTudo_Click);
            // 
            // ButtonEncherTudo
            // 
            this.ButtonEncherTudo.Enabled = false;
            this.ButtonEncherTudo.Location = new System.Drawing.Point(129, 88);
            this.ButtonEncherTudo.Name = "ButtonEncherTudo";
            this.ButtonEncherTudo.Size = new System.Drawing.Size(89, 23);
            this.ButtonEncherTudo.TabIndex = 10;
            this.ButtonEncherTudo.Text = "Encher (0 cl)";
            this.ButtonEncherTudo.UseVisualStyleBackColor = true;
            this.ButtonEncherTudo.Click += new System.EventHandler(this.ButtonEncherTudo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Bar.Properties.Resources.copo_1_1;
            this.pictureBox1.Location = new System.Drawing.Point(15, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 101);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // ComboBoxLiquido
            // 
            this.ComboBoxLiquido.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxLiquido.FormattingEnabled = true;
            this.ComboBoxLiquido.Items.AddRange(new object[] {
            "",
            "Coca-Cola",
            "7 Up",
            "Ice-Tea Pêsego",
            "Ice-Tea Limão",
            "Cerveja"});
            this.ComboBoxLiquido.Location = new System.Drawing.Point(99, 14);
            this.ComboBoxLiquido.Name = "ComboBoxLiquido";
            this.ComboBoxLiquido.Size = new System.Drawing.Size(138, 21);
            this.ComboBoxLiquido.TabIndex = 1;
            // 
            // ComboBoxCapacidade
            // 
            this.ComboBoxCapacidade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxCapacidade.FormattingEnabled = true;
            this.ComboBoxCapacidade.Items.AddRange(new object[] {
            "",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45"});
            this.ComboBoxCapacidade.Location = new System.Drawing.Point(99, 42);
            this.ComboBoxCapacidade.Name = "ComboBoxCapacidade";
            this.ComboBoxCapacidade.Size = new System.Drawing.Size(109, 21);
            this.ComboBoxCapacidade.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 248);
            this.Controls.Add(this.ComboBoxCapacidade);
            this.Controls.Add(this.ComboBoxLiquido);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.LabelVolume);
            this.Controls.Add(this.LabelCapacidade);
            this.Controls.Add(this.ButtonIniciarServico);
            this.Controls.Add(this.LabelLiquido);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelLiquido;
        private System.Windows.Forms.Label LabelCapacidadeUsada;
        private System.Windows.Forms.Button ButtonIniciarServico;
        private System.Windows.Forms.Label LabelCapacidade;
        private System.Windows.Forms.Button ButtonEsvaziar;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Label LabelBebida;
        private System.Windows.Forms.Label LabelVolume;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ButtonEsvaziarTudo;
        private System.Windows.Forms.Button ButtonEncherTudo;
        private System.Windows.Forms.ComboBox ComboBoxLiquido;
        private System.Windows.Forms.ComboBox ComboBoxCapacidade;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

